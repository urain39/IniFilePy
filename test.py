import os
import sys
import inifile as ini

if not os.path.exists("tests"):
    sys.exit(1)
os.chdir("tests")

print('===  Normaly Test  ===')

for test in os.listdir("./"):
    try:
        i = ini.IniFile(test)
    except ini.IniException:
        print("%s = [FAIL!]" % test)
        continue
    print("%s = [PASS!]" % test)

with open("dump.ini", "w") as dump_fp:
    i = ini.IniFile("config.ini")
    i.dump(dump_fp)

print ('=== Dump Method Test ===')

i = ini.IniFile("dump.ini")
i.dump(sys.stdout)

print('===  NoSection Test ===')

i = ini.IniFile('NoSection.ini')
i.dump()

